class Table {
    constructor (rows, columns) {
        this.rows = rows;
        this.columns = columns;
    }

    get addHTML(){
        let table = document.createElement('table');
        for (let i = 0; i<this.rows; i++){
            let row = table.insertRow(i);
            for (let j = 0; j<this.columns; j++){
                row.insertCell(j)
            }
        }
       document.body.appendChild(table)
    }

}

let t = new Table(30,30).addHTML;

document.body.addEventListener('click', function (event) {

    if (event.target === this){
        let table = document.querySelectorAll('td');
        table.forEach((item)=>{
            item.classList.toggle('black')
        })
    } else {
        event.target.classList.toggle('black');
    }

});

